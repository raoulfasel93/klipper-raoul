# This file contains common pin mappings for RAMPS (v1.3 and later)
# boards. RAMPS boards typically use a firmware compiled for the AVR
# atmega2560 (though other AVR chips are also possible).

# See the example.cfg file for a description of available parameters.

# Common EXP1 / EXP2 (display) pins

[stepper_x]
step_pin: ar54
dir_pin: !ar55
enable_pin: !ar38
step_distance: .005
#   Distance in mm that each step causes the axis to travel. This
#   parameter must be provided.
endstop_pin: ^ar3
#   Endstop switch detection pin. This parameter must be provided for
#   the X, Y, and Z steppers on cartesian style printers.
#endstop_pin: ^ar2
position_endstop: 0
position_max: 200
homing_speed: 50

[stepper_y]
step_pin: ar60
dir_pin: !ar61
enable_pin: !ar56
step_distance: .005
endstop_pin: ^ar14
#endstop_pin: ^ar15
position_endstop: 0
position_max: 200
homing_speed: 50

# Define a probe

######################################################################
# Bed probing hardware
######################################################################

# Z height probe. One may define this section to enable Z height
# probing hardware. When this section is enabled, PROBE and
# QUERY_PROBE extended g-code commands become available. The probe
# section also creates a virtual "probe:z_virtual_endstop" pin. One
# may set the stepper_z endstop_pin to this virtual pin on cartesian
# style printers that use the probe in place of a z endstop. If using
# "probe:z_virtual_endstop" then do not define a position_endstop in
# the stepper_z config section.
[probe]
pin: !ar18
#   Probe detection pin. This parameter must be provided.
#x_offset: 0.0
#   The distance (in mm) between the probe and the nozzle along the
#   x-axis. The default is 0.
y_offset: -50.0
#   The distance (in mm) between the probe and the nozzle along the
#   y-axis. The default is 0.
z_offset: 2.345
#   The distance (in mm) between the bed and the nozzle when the probe
#   triggers. This parameter must be provided.


[stepper_z]
step_pin: ar46
dir_pin: ar48
enable_pin: !ar62
step_distance: .00125
endstop_pin: probe:z_virtual_endstop
#endstop_pin: ^ar19
position_endstop: 0.5
position_max: 200

[extruder]
step_pin: ar26
dir_pin: !ar28
enable_pin: !ar24
step_distance: .003333333
nozzle_diameter: 0.400
filament_diameter: 1.750
heater_pin: ar10
sensor_type: NTC 100K beta 3950
sensor_pin: analog13
control: pid
pid_Kp: 36.50
pid_Ki: 5.43
pid_Kd: 61.30
min_temp: 0
max_temp: 250


[heater_bed]
heater_pin: ar8
sensor_type: EPCOS 100K B57560G104F
sensor_pin: analog14
control: pid
pid_Kp: 353.50
pid_Ki: 62.54
pid_Kd: 499.54
min_temp: 0
max_temp: 130

# Print cooling fan (omit section if fan not present).
[fan]
pin: ar9

[mcu]
serial: /dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0
pin_map: arduino

[printer]
kinematics: cartesian
max_velocity: 300
max_accel: 3000
max_z_velocity: 5
max_z_accel: 100


# This file serves as documentation for config parameters of
# additional devices that may be configured on a printer. The snippets
# in this file may be copied into the main printer.cfg file. See the
# "example.cfg" file for description of common config parameters.
#
# Note, where an extra config section creates additional pins, the
# section defining the pins must be listed in the config file before
# any sections using those pins.


######################################################################
# Bed leveling support
######################################################################

# Mesh Bed Leveling. One may define a [bed_mesh] config section
# to enable move transformations that offset the z axis based
# on a mesh generated from probed points. Note that bed_mesh
# and bed_tilt are incompatible, both cannot be defined.  When
# using a probe to home the z-axis, it is recommended to define
# a [homing_override] section in printer.cfg to home toward the
# center of the print area.
#
#  Visual Examples:
#   rectangular bed, probe_count = 3,3:
#               x---x---x (max_point)
#               |
#               x---x---x
#                       |
#   (min_point) x---x---x
#
#   round bed, round_probe_count = 5, bed_radius = r:
#                  x (0,r) end
#                /
#              x---x---x
#                        \
#   (-r,0) x---x---x---x---x (r,0)
#            \
#              x---x---x
#                    /
#                  x  (0,-r) start
#
[bed_mesh]
#speed: 50
#   The speed (in mm/s) of non-probing moves during the
#   calibration. The default is 50.
#horizontal_move_z: 5
#   The height (in mm) that the head should be commanded to move to
#   just prior to starting a probe operation. The default is 5.
#bed_radius:
#   Defines the radius to probe for round beds.  Note that the radius
#   is relative to the nozzle's origin, if using a probe be sure to
#   account for its offset.  This parameter must be provided for round
#   beds and omitted for rectangular beds.
min_point: 50,50
#   Defines the minimum x,y position to probe when for rectangular
#   beds. Note that this refers to the nozzle position, take care that
#   you do not define a point that will move the probe off of the bed.
#   This parameter must be provided for rectangular beds.
max_point: 100,100
#   Defines the maximum x,y position to probe when for rectangular
#   beds. Follow the same precautions as listed in min_point. Also note
#   that this does not necessarily define the last point probed, only
#   the maximum coordinate. This parameter must be provided.
probe_count: 3,3
#   For rectangular beds, this is a comma separate pair of integer
#   values (X,Y) defining the number of points to probe along each axis.
#   A single value is also valid, in which case that value will be applied
#   to both axes.  Default is 3,3.
#round_probe_count: 5
#   For round beds, this is integer value defines the maximum number of
#   points to probe along each axis. This value must be an odd number.
#   Default is 5.
#fade_start: 1.0
#   The gcode z position in which to start phasing out z-adjustment
#   when fade is enabled.  Default is 1.0.
#fade_end: 0.0
#   The gcode z position in which phasing out completes.  When set
#   to a value below fade_start, fade is disabled. It should be
#   noted that fade may add unwanted scaling along the z-axis of a
#   print.  If a user wishes to enable fade, a value of 10.0 is
#   recommended. Default is 0.0, which disables fade.
#fade_target:
#   The z position in which fade should converge. When this value is set
#   to a non-zero value it must be within the range of z-values in the mesh.
#   Users that wish to converge to the z homing position should set this to 0.
#   Default is the average z value of the mesh.
#split_delta_z: .025
#   The amount of Z difference (in mm) along a move that will
#   trigger a split. Default is .025.
#move_check_distance: 5.0
#   The distance (in mm) along a move to check for split_delta_z.
#   This is also the minimum length that a move can be split. Default
#   is 5.0.
#mesh_pps: 2,2
#   A comma separated pair of integers (X,Y) defining the number of
#   points per segment to interpolate in the mesh along each axis. A
#   "segment" can be defined as the space between each probed
#   point. The user may enter a single value which will be applied
#   to both axes.  Default is 2,2.
#algorithm: lagrange
#   The interpolation algorithm to use. May be either "lagrange"
#   or "bicubic". This option will not affect 3x3 grids, which
#   are forced to use lagrange sampling.  Default is lagrange.
#bicubic_tension: .2
#   When using the bicubic algorithm the tension parameter above
#   may be applied to change the amount of slope interpolated.
#   Larger numbers will increase the amount of slope, which
#   results in more curvature in the mesh. Default is .2.
#relative_reference_index:
#   A point index in the mesh to reference all z values to. Enabling
#   this parameter produces a mesh relative to the probed z position
#   at the provided index.



######################################################################
# Customized homing
######################################################################

# Safe Z homing. One may use this mechanism to home the Z axis at a
# specific XY coordinate. This is useful if the toolhead, for example
# has to move to the center of the bed before Z can be homed.
[safe_z_home]
home_xy_position: 100,100
#   A X,Y coordinate (e.g. 100,100) where the Z homing should be
#   performed. This parameter must be provided.
#speed: 50.0
#   Speed at which the toolhead is moved to the safe Z home coordinate.
#   The default is 50 mm/s
z_hop: 10.0
#   Lift the Z axis prior to homing. This is applied to any homing command,
#   even if it doesn't home the Z axis. If the Z axis is already homed and
#   the zhop would exceed the printer limits, the zhop is ignored. If a lift
#   has already been performed or the Z axis is known to be equally or higher
#   than this distance, the zhop is ignored. After homing Z completed, the
#   printhead is lifted to zhop, respecting the probe's z_offset.
#   The default is 0.0mm.
#z_hop_speed: 20.0
#   Speed at which the Z axis is lifted prior to homing. The default is 20mm/s.



######################################################################
# Optional G-Code features
######################################################################

# Support manually moving stepper motors for diagnostic purposes.
# Note, using this feature may place the printer in an invalid state -
# see docs/G-Codes.md for important details.
#[force_move]
#enable_force_move: False
#   Set to true to enable FORCE_MOVE and SET_KINEMATIC_POSITION
#   extended G-Code commands. The default is false.

# Pause/Resume functionality with support of position capture and restore
#[pause_resume]
#recover_velocity: 50.
#   When capture/restore is enabled, the speed at which to return to
#   the captured position (in mm/s).  Default is 50.0 mm/s.

# Firmware filament retraction. This enables G10 (retract) and G11
# (unretract) GCODE commands issued by many slicers. The parameters
# below provide startup defaults, although the values can be adjusted
# via the SET_RETRACTION command, allowing per-filament settings and
# runtime tuning.
#[firmware_retraction]
#retract_length: 0
#   The length of filament (in mm) to retract when G10 is activated, and to
#   unretract when G11 is activated (but see unretract_extra_length below).
#   The default is 0 mm.
#retract_speed: 20
#   The speed of retraction, in mm/s. The default is 20 mm/s.
#unretract_extra_length: 0
#   The length (in mm) of *additional* filament to add when unretracting.
#unretract_speed: 10
#   The speed of unretraction, in mm/s. The default is 10 mm/s.





######################################################################
# Additional fans
######################################################################

# Heater cooling fans (one may define any number of sections with a
# "heater_fan" prefix). A "heater fan" is a fan that will be enabled
# whenever its associated heater is active. By default, a heater_fan
# has a shutdown_speed equal to max_power.
[heater_fan nozzle_fan]
pin: ar7
#max_power:
#shutdown_speed:
#cycle_time:
#hardware_pwm:
#kick_start_time:
#   See the "fan" section in example.cfg for a description of the
#   above parameters.
heater: extruder
#   Name of the config section defining the heater that this fan is
#   associated with. If a comma separated list of heater names is
#   provided here, then the fan will be enabled when any of the given
#   heaters are enabled. The default is "extruder".
heater_temp: 90
#   A temperature (in Celsius) that the heater must drop below before
#   the fan is disabled. The default is 50 Celsius.
#fan_speed: 1.0
#   The fan speed (expressed as a value from 0.0 to 1.0) that the fan
#   will be set to when its associated heater is enabled. The default
#   is 1.0

# Controller cooling fan (one may define any number of sections with a
# "controller_fan" prefix). A "controller fan" is a fan that will be
# enabled whenever its associated heater or any configured stepper
# driver is active. The fan will stop, whenever an idle_timeout is
# reached to ensure no overheating will occur after deactivating a
# watched component.
#[controller_fan my_controller_fan]
#pin:
#max_power:
#shutdown_speed:
#cycle_time:
#hardware_pwm:
#kick_start_time:
#   See the "fan" section in example.cfg for a description of the
#   above parameters.
#idle_timeout:
#   The ammount of time (in seconds) after a stepper driver or heater
#   was active and the fan should be kept running. The default
#   is 30 seconds.
#idle_speed:
#   The fan speed (expressed as a value from 0.0 to 1.0) that the fan
#   will be set to when a heater or stepper driver was active and before
#   the idle_timeout is reached. This must be greater or equal
#   max_power. The default is max_power
#heater:
#   Name of the config section defining the heater that this fan is
#   associated with. If a comma separated list of heater names is
#   provided here, then the fan will be enabled when any of the given
#   heaters are enabled. The default is "extruder".

######################################################################
# Additional servos, LEDs, buttons, and other pins
######################################################################
# Run-time configurable output pins (one may define any number of
# sections with an "output_pin" prefix). Pins configured here will be
# setup as output pins and one may modify them at run-time using
# "SET_PIN PIN=my_pin VALUE=.1" type extended g-code commands.
[output_pin psupower]
# pin for power supply atx x360
pin: ar5
# power on
[gcode_macro M80]
gcode: SET_PIN PIN=psupower VALUE=1

# power off
[gcode_macro M81]
gcode: SET_PIN PIN=psupower VALUE=0


# Servos (one may define any number of sections with a "servo"
# prefix). The servos may be controlled using the SET_SERVO g-code
# command. For example: SET_SERVO SERVO=my_servo ANGLE=180
#[servo my_servo]
#pin: ar7
#   PWM output pin controlling the servo. This parameter must be
#   provided.
#maximum_servo_angle: 180
#   The maximum angle (in degrees) that this servo can be set to. The
#   default is 180 degrees.
#minimum_pulse_width: 0.001
#   The minimum pulse width time (in seconds). This should correspond
#   with an angle of 0 degrees. The default is 0.001 seconds.
#maximum_pulse_width: 0.002
#   The maximum pulse width time (in seconds). This should correspond
#   with an angle of maximum_servo_angle. The default is 0.002
#   seconds.
#initial_angle: 70
#   Initial angle to set the servo to when the mcu resets.  Must be between
#   0 and maximum_servo_angle  This parameter is optional.  If both
#   initial_angle and initial_pulse_width are set initial_angle will be used.
#initial_pulse_width: 0.0015
#   Initial pulse width time (in seconds) to set the servo to when
#   the mcu resets.  Must be between minimum_pulse_width and maximum_pulse_width.
#   This parameter is optional.  If both initial_angle and initial_pulse_width
#   are set initial_angle will be used
#enable: True
#   Enable or disable servo. It can be enabled or disabled later using
#   SET_SERVO SERVO=my_servo ENABLE=<0|1> g-command. The default is True (=enabled)

# Neopixel (aka WS2812) LED support (one may define any number of
# sections with a "neopixel" prefix). One may set the LED color via
# "SET_LED LED=my_neopixel RED=0.1 GREEN=0.1 BLUE=0.1" type extended
# g-code commands.
#[neopixel my_neopixel]
#pin:
#   The pin connected to the neopixel. This parameter must be
#   provided.
#chain_count:
#   The number of Neopixel chips that are "daisy chained" to the
#   provided pin. The default is 1 (which indices only a single
#   Neopixel is connected to the pin).
#color_order_GRB: True
#   Set the pixel order to green, red, blue. If using the WS2811 chip
#   (in 800Khz mode) then set this to False. The default is True.
#initial_RED: 0.0
#initial_GREEN: 0.0
#initial_BLUE: 0.0
#   Sets the initial LED color of the Neopixel. Each value should be
#   between 0.0 and 1.0. The default for each color is 0.


######################################################################
# TMC stepper driver configuration
######################################################################

# Configure a TMC2130 stepper motor driver via SPI bus. To use this
# feature, define a config section with a "tmc2130" prefix followed by
# the name of the corresponding stepper config section (for example,
# "[tmc2130 stepper_x]"). This also creates a
# "tmc2130_stepper_x:virtual_enable" virtual pin which may be used as
# the stepper's enable_pin (for enabling the driver via an SPI
# message).
#[tmc2130 stepper_x]
#cs_pin:
#   The pin corresponding to the TMC2130 chip select line. This pin
#   will be set to low at the start of SPI messages and raised to high
#   after the message completes. This parameter must be provided.
#spi_bus:
#spi_speed:
#spi_software_sclk_pin:
#spi_software_mosi_pin:
#spi_software_miso_pin:
#   These optional parameters allow one to customize the SPI settings
#   used to communicate with the chip.
#microsteps:
#   The number of microsteps to configure the driver to use. Valid
#   values are 1, 2, 4, 8, 16, 32, 64, 128, 256. This parameter must
#   be provided.
#interpolate: True
#   If true, enable step interpolation (the driver will internally
#   step at a rate of 256 micro-steps). The default is True.
#run_current:
#   The amount of current (in amps) to configure the driver to use
#   during stepper movement. This parameter must be provided.
#hold_current:
#   The amount of current (in amps) to configure the driver to use
#   when the stepper is not moving. The default is to use the same
#   value as run_current.
#sense_resistor: 0.110
#   The resistance (in ohms) of the motor sense resistor. The default
#   is 0.110 ohms.
#stealthchop_threshold: 0
#   The velocity (in mm/s) to set the "stealthChop" threshold to. When
#   set, "stealthChop" mode will be enabled if the stepper motor
#   velocity is below this value. The default is 0, which disables
#   "stealthChop" mode.
#driver_IHOLDDELAY: 8
#driver_TPOWERDOWN: 0
#driver_TBL: 1
#driver_TOFF: 4
#driver_HEND: 7
#driver_HSTRT: 0
#driver_PWM_AUTOSCALE: True
#driver_PWM_FREQ: 1
#driver_PWM_GRAD: 4
#driver_PWM_AMPL: 128
#driver_SGT: 0
#   Set the given register during the configuration of the TMC2130
#   chip. This may be used to set custom motor parameters. The
#   defaults for each parameter are next to the parameter name in the
#   above list.
#diag1_pin:
#   The micro-controller pin attached to the DIAG1 line of the TMC2130
#   chip. Setting this creates a "tmc2130_stepper_x:virtual_endstop"
#   virtual pin which may be used as the stepper's endstop_pin. Doing
#   this enables "sensorless homing". (Be sure to also set driver_SGT
#   to an appropriate sensitivity value.) The default is to not enable
#   sensorless homing. See docs/Sensorless_Homing.md for details on how
#   to configure this.

# Configure a TMC2208 (or TMC2224) stepper motor driver via single
# wire UART. To use this feature, define a config section with a
# "tmc2208" prefix followed by the name of the corresponding stepper
# config section (for example, "[tmc2208 stepper_x]"). This also
# creates a "tmc2208_stepper_x:virtual_enable" virtual pin which may
# be used as the stepper's enable_pin (for enabling the driver via a
# UART message).
#[tmc2208 stepper_x]
#uart_pin:
#   The pin connected to the TMC2208 PDN_UART line. This parameter
#   must be provided.
#tx_pin:
#   If using separate receive and transmit lines to communicate with
#   the driver then set uart_pin to the receive pin and tx_pin to the
#   transmit pin. The default is to use uart_pin for both reading and
#   writing.
#select_pins:
#   A comma separated list of pins to set prior to accessing the
#   tmc2208 UART. This may be useful for configuring an analog mux for
#   UART communication. The default is to not configure any pins.
#microsteps:
#   The number of microsteps to configure the driver to use. Valid
#   values are 1, 2, 4, 8, 16, 32, 64, 128, 256. This parameter must
#   be provided.
#interpolate: True
#   If true, enable step interpolation (the driver will internally
#   step at a rate of 256 micro-steps). The default is True.
#run_current:
#   The amount of current (in amps) to configure the driver to use
#   during stepper movement. This parameter must be provided.
#hold_current:
#   The amount of current (in amps) to configure the driver to use
#   when the stepper is not moving. The default is to use the same
#   value as run_current.
#sense_resistor: 0.110
#   The resistance (in ohms) of the motor sense resistor. The default
#   is 0.110 ohms.
#stealthchop_threshold: 0
#   The velocity (in mm/s) to set the "stealthChop" threshold to. When
#   set, "stealthChop" mode will be enabled if the stepper motor
#   velocity is below this value. The default is 0, which disables
#   "stealthChop" mode.
#driver_IHOLDDELAY: 8
#driver_TPOWERDOWN: 20
#driver_TBL: 2
#driver_TOFF: 3
#driver_HEND: 0
#driver_HSTRT: 5
#driver_PWM_AUTOGRAD: True
#driver_PWM_AUTOSCALE: True
#driver_PWM_LIM: 12
#driver_PWM_REG: 8
#driver_PWM_FREQ: 1
#driver_PWM_GRAD: 14
#driver_PWM_OFS: 36
#   Set the given register during the configuration of the TMC2208
#   chip. This may be used to set custom motor parameters. The
#   defaults for each parameter are next to the parameter name in the
#   above list.

# Configure a TMC2209 stepper motor driver via single wire UART. To
# use this feature, define a config section with a "tmc2209" prefix
# followed by the name of the corresponding stepper config section
# (for example, "[tmc2209 stepper_x]"). This also creates a
# "tmc2209_stepper_x:virtual_enable" virtual pin which may be used as
# the stepper's enable_pin (for enabling the driver via a UART
# message).
#[tmc2209 stepper_x]
#uart_pin:
#tx_pin:
#select_pins:
#microsteps:
#interpolate: True
#run_current:
#hold_current:
#sense_resistor: 0.110
#stealthchop_threshold: 0
#   See the tmc2208 section above for the definition of these
#   parameters.
#uart_address:
#   The address of the TMC2209 chip for UART messages (an integer
#   between 0 and 3). This is typically used when multiple TMC2209
#   chips are connected to the same UART pin. The default is zero.
#driver_IHOLDDELAY: 8
#driver_TPOWERDOWN: 20
#driver_TBL: 2
#driver_TOFF: 3
#driver_HEND: 0
#driver_HSTRT: 5
#driver_PWM_AUTOGRAD: True
#driver_PWM_AUTOSCALE: True
#driver_PWM_LIM: 12
#driver_PWM_REG: 8
#driver_PWM_FREQ: 1
#driver_PWM_GRAD: 14
#driver_PWM_OFS: 36
#driver_SGTHRS: 0
#   Set the given register during the configuration of the TMC2209
#   chip. This may be used to set custom motor parameters. The
#   defaults for each parameter are next to the parameter name in the
#   above list.
#diag_pin:
#   The micro-controller pin attached to the DIAG line of the TMC2209
#   chip. Setting this creates a "tmc2209_stepper_x:virtual_endstop"
#   virtual pin which may be used as the stepper's endstop_pin. Doing
#   this enables "sensorless homing". (Be sure to also set
#   driver_SGTHRS to an appropriate sensitivity value.) The default is
#   to not enable sensorless homing.

# Configure a TMC2660 stepper motor driver via SPI bus. To use this
# feature, define a config section with a tmc2660 prefix followed by
# the name of the corresponding stepper config section (for example,
# "[tmc2660 stepper_x]"). This also creates a
# "tmc2660_stepper_x:virtual_enable" virtual pin which may be used as
# the stepper's enable_pin (for enabling the driver via an SPI
# message).
#[tmc2660 stepper_x]
#cs_pin:
#   The pin corresponding to the TMC2660 chip select line. This pin
#   will be set to low at the start of SPI messages and set to high
#   after the message transfer completes. This parameter must be provided.
#spi_bus:
#   Select the SPI bus the TMC2660 stepper driver is connected to.
#   This depends on the physical connections on your board, as well as
#   the SPI implementation of your particular micro-controller. The
#   default is to use the default micro-controller spi bus.
#spi_speed: 4000000
#   SPI bus frequency used to communicate with the TMC2660 stepper
#   driver. The default is 4000000.
#spi_software_sclk_pin:
#spi_software_mosi_pin:
#spi_software_miso_pin:
#   These optional parameters allow one to customize the SPI settings
#   used to communicate with the chip.
#microsteps:
#   The number of microsteps to configure the driver to use. Valid
#   values are 1, 2, 4, 8, 16, 32, 64, 128, 256. This parameter must
#   be provided.
#interpolate: True
#   If true, enable step interpolation (the driver will internally
#   step at a rate of 256 micro-steps). This only works if microsteps
#   is set to 16. The default is True.
#run_current:
#   The amount of current (in ampere) used by the driver during stepper
#   movement. This parameter must be provided.
#sense_resistor:
#   The resistance (in ohms) of the motor sense resistor. This parameter
#   must be provided.
#idle_current_percent: 100
#   The percentage of the run_current the stepper driver will be
#   lowered to when the idle timeout expires (you need to set up the
#   timeout using a [idle_timeout] config section). The current will
#   be raised again once the stepper has to move again. Make sure to
#   set this to a high enough value such that the steppers do not lose
#   their position. There is also small delay until the  current is
#   raised again, so take this into account when commanding fast moves
#   while the stepper is idling. The default is 100 (no reduction).
#driver_TBL: 2
#driver_RNDTF: 0
#driver_HDEC: 0
#driver_CHM: 0
#driver_HEND: 3
#driver_HSTRT: 3
#driver_TOFF: 4
#driver_SEIMIN: 0
#driver_SEDN: 0
#driver_SEMAX: 0
#driver_SEUP: 0
#driver_SEMIN: 0
#driver_SFILT: 1
#driver_SGT: 0
#driver_SLPH: 0
#driver_SLPL: 0
#driver_DISS2G: 0
#driver_TS2G: 3
#   Set the given parameter during the configuration of the TMC2660
#   chip. This may be used to set custom driver parameters. The
#   defaults for each parameter are next to the parameter name in the
#   list above. See the TMC2660 datasheet about what each parameter
#   does and what the restrictions on parameter combinations are.
#   Be especially aware of the CHOPCONF register, where setting CHM to
#   either 0 or one will lead to layout changes (the first bit of HDEC)
#   is interpreted as the MSB of HSTRT in this case).

# Configure a TMC5160 stepper motor driver via SPI bus. To use this
# feature, define a config section with a "tmc5160" prefix followed by
# the name of the corresponding stepper config section (for example,
# "[tmc5160 stepper_x]"). This also creates a
# "tmc5160_stepper_x:virtual_enable" virtual pin which may be used as
# the stepper's enable_pin (for enabling the driver via an SPI
# message).
#[tmc5160 stepper_x]
#cs_pin:
#   The pin corresponding to the TMC5160 chip select line. This pin
#   will be set to low at the start of SPI messages and raised to high
#   after the message completes. This parameter must be provided.
#spi_bus:
#spi_speed:
#spi_software_sclk_pin:
#spi_software_mosi_pin:
#spi_software_miso_pin:
#   These optional parameters allow one to customize the SPI settings
#   used to communicate with the chip.
#microsteps:
#   The number of microsteps to configure the driver to use. Valid
#   values are 1, 2, 4, 8, 16, 32, 64, 128, 256. This parameter must
#   be provided.
#interpolate: True
#   If true, enable step interpolation (the driver will internally
#   step at a rate of 256 micro-steps). The default is True.
#run_current:
#   The amount of current (in amps) to configure the driver to use
#   during stepper movement. This parameter must be provided.
#hold_current:
#   The amount of current (in amps) to configure the driver to use
#   when the stepper is not moving. The default is to use the same
#   value as run_current.
#sense_resistor: 0.075
#   The resistance (in ohms) of the motor sense resistor. The default
#   is 0.075 ohms.
#stealthchop_threshold: 0
#   The velocity (in mm/s) to set the "stealthChop" threshold to. When
#   set, "stealthChop" mode will be enabled if the stepper motor
#   velocity is below this value. The default is 0, which disables
#   "stealthChop" mode. Try to reexperience this with tmc5160.
#   Values can be much higher than other tmcs.
#driver_IHOLDDELAY: 6
#driver_TPOWERDOWN: 10
#driver_TBL: 2
#driver_TOFF: 3
#driver_HEND: 2
#driver_HSTRT: 5
#driver_FD3: 0
#driver_TPFD: 4
#driver_CHM: 0
#driver_VHIGHFS: 0
#driver_VHIGHCHM: 0
#driver_DISS2G: 0
#driver_DISS2VS: 0
#driver_PWM_AUTOSCALE: True
#driver_PWM_AUTOGRAD: True
#driver_PWM_FREQ: 1
#driver_FREEWHEEL: 0
#driver_PWM_GRAD: 0
#driver_PWM_OFS: 30
#driver_PWM_REG: 4
#driver_PWM_LIM: 12
#driver_SGT: 0
#driver_SEMIN: 0
#driver_SEUP: 0
#driver_SEMAX: 0
#driver_SEDN: 0
#driver_SEIMIN: 0
#driver_SFILT: 0
#   Set the given register during the configuration of the TMC5160
#   chip. This may be used to set custom motor parameters. The
#   defaults for each parameter are next to the parameter name in the
#   above list.
#diag1_pin:
#   The micro-controller pin attached to the DIAG1 line of the TMC5160
#   chip. Setting this creates a "tmc5160_stepper_x:virtual_endstop"
#   virtual pin which may be used as the stepper's endstop_pin. Doing
#   this enables "sensorless homing". (Be sure to also set driver_SGT
#   to an appropriate sensitivity value.) The default is to not enable
#   sensorless homing. See docs/Sensorless_Homing.md for details on how
#   to configure this.


######################################################################
# Run-time stepper motor current configuration
######################################################################

# Statically configured AD5206 digipots connected via SPI bus (one may
# define any number of sections with an "ad5206" prefix).
#[ad5206 my_digipot]
#enable_pin:
#   The pin corresponding to the AD5206 chip select line. This pin
#   will be set to low at the start of SPI messages and raised to high
#   after the message completes. This parameter must be provided.
#spi_bus:
#spi_speed:
#spi_software_sclk_pin:
#spi_software_mosi_pin:
#spi_software_miso_pin:
#   These optional parameters allow one to customize the SPI settings
#   used to communicate with the chip.
#channel_1:
#channel_2:
#channel_3:
#channel_4:
#channel_5:
#channel_6:
#   The value to statically set the given AD5206 channel to. This is
#   typically set to a number between 0.0 and 1.0 with 1.0 being the
#   highest resistance and 0.0 being the lowest resistance. However,
#   the range may be changed with the 'scale' parameter (see below).
#   If a channel is not specified then it is left unconfigured.
#scale:
#   This parameter can be used to alter how the 'channel_x' parameters
#   are interpreted. If provided, then the 'channel_x' parameters
#   should be between 0.0 and 'scale'. This may be useful when the
#   AD5206 is used to set stepper voltage references. The 'scale' can
#   be set to the equivalent stepper amperage if the AD5206 were at
#   its highest resistance, and then the 'channel_x' parameters can be
#   specified using the desired amperage value for the stepper. The
#   default is to not scale the 'channel_x' parameters.

# Statically configured MCP4451 digipot connected via I2C bus (one may
# define any number of sections with an "mcp4451" prefix).
#[mcp4451 my_digipot]
#i2c_mcu: mcu
#   The name of the micro-controller that the MCP4451 chip is
#   connected to. The default is "mcu".
#i2c_address:
#   The i2c address that the chip is using on the i2c bus. This
#   parameter must be provided.
#wiper_0:
#wiper_1:
#wiper_2:
#wiper_3:
#   The value to statically set the given MCP4451 "wiper" to. This is
#   typically set to a number between 0.0 and 1.0 with 1.0 being the
#   highest resistance and 0.0 being the lowest resistance. However,
#   the range may be changed with the 'scale' parameter (see
#   below). If a wiper is not specified then it is left unconfigured.
#scale:
#   This parameter can be used to alter how the 'wiper_x' parameters
#   are interpreted. If provided, then the 'wiper_x' parameters should
#   be between 0.0 and 'scale'. This may be useful when the MCP4451 is
#   used to set stepper voltage references. The 'scale' can be set to
#   the equivalent stepper amperage if the MCP4451 were at its highest
#   resistance, and then the 'wiper_x' parameters can be specified
#   using the desired amperage value for the stepper. The default is
#   to not scale the 'wiper_x' parameters.

# Statically configured MCP4728 digital-to-analog converter connected
# via I2C bus (one may define any number of sections with an "mcp4728"
# prefix).
#[mcp4728 my_dac]
#i2c_mcu: mcu
#   The name of the micro-controller that the MCP4451 chip is
#   connected to. The default is "mcu".
#i2c_address: 96
#   The i2c address that the chip is using on the i2c bus. The default
#   is 96.
#channel_a:
#channel_b:
#channel_c:
#channel_d:
#   The value to statically set the given MCP4728 channel to. This is
#   typically set to a number between 0.0 and 1.0 with 1.0 being the
#   highest voltage (2.048V) and 0.0 being the lowest voltage. However,
#   the range may be changed with the 'scale' parameter (see
#   below). If a channel is not specified then it is left
#   unconfigured.
#scale:
#   This parameter can be used to alter how the 'channel_x' parameters
#   are interpreted. If provided, then the 'channel_x' parameters
#   should be between 0.0 and 'scale'. This may be useful when the
#   MCP4728 is used to set stepper voltage references. The 'scale' can
#   be set to the equivalent stepper amperage if the MCP4728 were at
#   its highest voltage (2.048V), and then the 'channel_x' parameters
#   can be specified using the desired amperage value for the
#   stepper. The default is to not scale the 'channel_x' parameters.

# Statically configured MCP4018 digipot connected via two gpio "bit
# banging" pins (one may define any number of sections with an
# "mcp4018" prefix).
#[mcp4018 my_digipot]
#scl_pin:
#   The SCL "clock" pin. This parameter must be provided.
#sda_pin:
#   The SDA "data" pin. This parameter must be provided.
#wiper:
#   The value to statically set the given MCP4018 "wiper" to. This is
#   typically set to a number between 0.0 and 1.0 with 1.0 being the
#   highest resistance and 0.0 being the lowest resistance. However,
#   the range may be changed with the 'scale' parameter (see
#   below). This parameter must be provided.
#scale:
#   This parameter can be used to alter how the 'wiper' parameter is
#   interpreted. If provided, then the 'wiper' parameter should be
#   between 0.0 and 'scale'. This may be useful when the MCP4018 is
#   used to set stepper voltage references. The 'scale' can be set to
#   the equivalent stepper amperage if the MCP4018 is at its highest
#   resistance, and then the 'wiper' parameter can be specified using
#   the desired amperage value for the stepper. The default is to not
#   scale the 'wiper' parameter.



######################################################################
# Filament sensors
######################################################################

# Filament Switch Sensor.  Support for filament insert and runout detection
# using a switch sensor, such as an endstop switch.
#[filament_switch_sensor my_sensor]
#pause_on_runout: True
#   When set to True, a PAUSE will execute immediately after a runout
#   is detected. Note that if pause_on_runout is False and the
#   runout_gcode is omitted then runout detection is disabled. Default
#   is True.
#runout_gcode:
#   A list of G-Code commands to execute after a filament runout is
#   detected. See docs/Command_Templates.md for G-Code format. If
#   pause_on_runout is set to True this G-Code will run after the
#   PAUSE is complete. The default is not to run any G-Code commands.
#insert_gcode:
#   A list of G-Code commands to execute after a filament insert is
#   detected. See docs/Command_Templates.md for G-Code format. The
#   default is not to run any G-Code commands, which disables insert
#   detection.
#event_delay: 3.0
#   The minimum amount of time in seconds to delay between events.
#   Events triggered during this time period will be silently
#   ignored. The default is 3 seconds.
#pause_delay: 0.5
#   The amount of time to delay, in seconds, between the pause command
#   dispatch and execution of the runout_gcode.  It may be useful to
#   increase this delay if Octoprint exhibits strange pause behavior.
#   Default is 0.5 seconds.
#switch_pin:
#   The pin on which the switch is connected. This parameter must be
#   provided.

# TSLl401CL Based Filament Width Sensor
#[tsl1401cl_filament_width_sensor]
#pin: analog5
#default_nominal_filament_diameter: 1.75 # (mm)
#   Maximum allowed filament diameter difference as mm
#max_difference: 0.2
#   The distance from sensor to the melting chamber as mm
#measurement_delay: 100



